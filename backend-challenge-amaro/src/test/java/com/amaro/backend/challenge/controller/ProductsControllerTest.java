package com.amaro.backend.challenge.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.amaro.backend.challenge.product.ProductRequest;
import com.amaro.backend.challenge.product.ProductResponse;
import com.amaro.backend.challenge.product.ProductSearchRequest;
import com.amaro.backend.challenge.product.ProductSearchResponse;
import com.amaro.backend.challenge.product.Products;
import com.amaro.backend.challenge.service.ProductsService;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductsController.class)
public class ProductsControllerTest {

	@Autowired 
	MockMvc mvc;

	@MockBean
	ProductsService productsService;
	
	@Test
	public void testProductsTags() {
		ProductRequest productRequest = request();
		when(productsService.findProductTags(productRequest.getProducts())).thenReturn(setupResponse());
        try {
        	ObjectMapper mapper = new ObjectMapper();
        	String jsonInString = mapper.writeValueAsString(productRequest);
        	mvc.perform(post("http://localhost:8080/amaro-backend-challenge/product-tags")
		            .contentType(MediaType.APPLICATION_JSON)
		            .content(jsonInString))
		            .andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testProductsSimilarity() {	

		ProductSearchRequest productSearchRequest = new ProductSearchRequest();
		productSearchRequest.setProducts(setupResponse());
		when(productsService.findProductSimilarity(searchProduct(),setupResponse())).thenReturn(searchProductResponse());
        try {
        	ObjectMapper mapper = new ObjectMapper();
        	String jsonInString = mapper.writeValueAsString(productSearchRequest);
        	mvc.perform(post("http://localhost:8080/amaro-backend-challenge/product-tags").param("productId", "8314")
		            .contentType(MediaType.APPLICATION_JSON)
		            .content(jsonInString))
		            .andExpect(status().isOk());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	public ProductRequest request() {

		ProductRequest productRequest = new ProductRequest();
		Products product1 = new Products();
		Vector<String> productVector1 = new Vector<String>();
		productVector1.add("neutro");
		productVector1.add("veludo");
		productVector1.add("couro");
		product1.setId("100");
		product1.setName("test100");
		product1.setTags(productVector1);

		Products product2 = new Products();
		Vector<String> productVector2 = new Vector<String>();
		productVector2.add("basics");
		productVector2.add("festa");
		productVector2.add("workwear");
		product2.setId("200");
		product2.setName("test200");
		product2.setTags(productVector2);

		Products product3 = new Products();
		Vector<String> productVector3 = new Vector<String>();
		productVector3.add("inverno");
		productVector3.add("boho");
		productVector3.add("estampas");
		product3.setId("300");
		product3.setName("test300");
		product3.setTags(productVector3);

		Vector<Products> productsVector = new Vector<Products>();
		productsVector.add(product1);
		productsVector.add(product2);
		productsVector.add(product3);
		productRequest.setProducts(productsVector);

		return productRequest;
	}

	public List<ProductResponse> setupResponse() {

		int v1[] = {1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		int v2[] = {0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		int v3[] = {0,0,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0};
		List<ProductResponse> productResponse = new ArrayList<ProductResponse>();
		ProductResponse productResponse1 = new ProductResponse();
		Vector<String> productVector1 = new Vector<String>();
		productVector1.add("neutro");
		productVector1.add("veludo");
		productVector1.add("couro");
		Vector<Integer> productTagsVector1 = new Vector<Integer>();
		productTagsVector1.addAll((Collection)Arrays.asList(v1));
		productResponse1.setId("100");
		productResponse1.setName("test100");
		productResponse1.setTags(productVector1);

		ProductResponse productResponse2 = new ProductResponse();
		Vector<String> productVector2 = new Vector<String>();
		productVector2.add("basics");
		productVector2.add("festa");
		productVector2.add("workwear");
		Vector<Integer> productTagsVector2 = new Vector<Integer>();
		productTagsVector2.addAll((Collection)Arrays.asList(v2));
		productResponse2.setId("200");
		productResponse2.setName("test200");
		productResponse2.setTags(productVector2);

		ProductResponse productResponse3 = new ProductResponse();
		Vector<String> productVector3 = new Vector<String>();
		productVector3.add("inverno");
		productVector3.add("boho");
		productVector3.add("estampas");
		Vector<Integer> productTagsVector3 = new Vector<Integer>();
		productTagsVector3.addAll((Collection)Arrays.asList(v3));
		productResponse3.setId("300");
		productResponse3.setName("test300");
		productResponse3.setTags(productVector3);

		productResponse.add(productResponse1);
		productResponse.add(productResponse2);
		productResponse.add(productResponse3);

		return productResponse;
	}

	public ProductResponse searchProduct()
	{
		int v1[] = {1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		ProductResponse productResponse1 = new ProductResponse();
		Vector<String> productVector1 = new Vector<String>();
		productVector1.add("neutro");
		productVector1.add("veludo");
		productVector1.add("couro");
		Vector<Integer> productTagsVector1 = new Vector<Integer>();
		productTagsVector1.addAll((Collection)Arrays.asList(v1));
		productResponse1.setId("100");
		productResponse1.setName("test100");
		productResponse1.setTags(productVector1);
		return productResponse1;
	}

	public List<ProductSearchResponse> searchProductResponse()
	{
		List<ProductSearchResponse> productSearchResponse = new ArrayList<ProductSearchResponse>();
		ProductSearchResponse productSearchResponse1 = new ProductSearchResponse();
		productSearchResponse1.setId("8314");
		productSearchResponse1.setName("VESTIDO PLISSADO ACINTURADO");
		productSearchResponse1.setSimilarity(0.3090169943749474);
		ProductSearchResponse productSearchResponse2 = new ProductSearchResponse();
		productSearchResponse2.setId("8314");
		productSearchResponse2.setName("VESTIDO PLISSADO ACINTURADO");
		productSearchResponse2.setSimilarity(0.3090169943749474);
		ProductSearchResponse productSearchResponse3 = new ProductSearchResponse();
		productSearchResponse3.setId("8314");
		productSearchResponse3.setName("VESTIDO PLISSADO ACINTURADO");
		productSearchResponse3.setSimilarity(0.3090169943749474);
		productSearchResponse.add(productSearchResponse3);
		productSearchResponse.add(productSearchResponse2);
		productSearchResponse.add(productSearchResponse1);
		return productSearchResponse;
	}

}
