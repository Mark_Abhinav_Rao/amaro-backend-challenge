package com.amaro.backend.challenge.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amaro.backend.challenge.product.ProductResponse;
import com.amaro.backend.challenge.product.ProductSearchResponse;
import com.amaro.backend.challenge.product.Products;

public interface ProductsService {

	public Map<String,ProductResponse> findProductTags(List<Products> productRequest);
	
	public List<ProductSearchResponse> findProductSimilarity(ProductResponse searchProduct, HashMap<Integer, ProductResponse> products);
	
}
