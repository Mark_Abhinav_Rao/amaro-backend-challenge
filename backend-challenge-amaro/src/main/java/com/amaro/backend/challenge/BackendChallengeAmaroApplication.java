package com.amaro.backend.challenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendChallengeAmaroApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendChallengeAmaroApplication.class, args);
	}

}
