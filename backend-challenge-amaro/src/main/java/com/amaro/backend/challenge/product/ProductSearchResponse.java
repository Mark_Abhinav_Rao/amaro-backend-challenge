package com.amaro.backend.challenge.product;

public class ProductSearchResponse implements Comparable<ProductSearchResponse> {

	String id;
	String name;
	double similarity;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getSimilarity() {
		return similarity;
	}
	public void setSimilarity(double similarity) {
		this.similarity = similarity;
	}

	@Override
	public int compareTo(ProductSearchResponse productSearchResponse) {

		if(this.similarity<=productSearchResponse.similarity)    
			return 1;    
		else    
			return -1;  
	}
}
