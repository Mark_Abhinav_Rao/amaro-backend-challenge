package com.amaro.backend.challenge.product;

import java.util.Vector;

public class ProductResponse{

	String id;
	String name;
	Vector<String> tags = new Vector<String>();
	Vector<Integer> tagsVector = new Vector<Integer>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<String> getTags() {
		return tags;
	}
	public void setTags(Vector<String> tags) {
		this.tags = tags;
	}
	public Vector<Integer> getTagsVector() {
		return tagsVector;
	}
	public void setTagsVector(Vector<Integer> tagsVector) {
		this.tagsVector = tagsVector;
	}
}
