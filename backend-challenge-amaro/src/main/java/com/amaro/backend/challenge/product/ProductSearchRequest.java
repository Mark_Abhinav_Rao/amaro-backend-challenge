package com.amaro.backend.challenge.product;


import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductSearchRequest {

	 @JsonProperty("products")
	List<ProductResponse> products;

	public List<ProductResponse> getProducts() {
		return products;
	}

	public void setProducts(List<ProductResponse> products) {
		this.products = products;
	}
	
}
