package com.amaro.backend.challenge.controller;


import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amaro.backend.challenge.product.ProductRequest;
import com.amaro.backend.challenge.product.ProductResponse;
import com.amaro.backend.challenge.product.ProductSearchResponse;
import com.amaro.backend.challenge.service.ProductsService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Controller 
@RequestMapping("/amaro-backend-challenge")
@ResponseBody
public class ProductsController {

	private static final String productSimilarity = "/product-similarity";
	private static final String productTags = "/product-tags";

	@Resource
	ProductsService productsService;

	@RequestMapping(value = productTags, method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes =  MediaType.APPLICATION_JSON_VALUE)
	public Map<String, ProductResponse> productTags(@RequestBody final ProductRequest products)
	{
		return productsService.findProductTags(products.getProducts());
	}

	@RequestMapping(value = productSimilarity, method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE,consumes =  MediaType.APPLICATION_JSON_VALUE)
	public List<ProductSearchResponse> productSimilarity(@RequestBody final String productsJson,@RequestParam final String productId)
	{
		final Gson gson = new Gson();
		Type type = new TypeToken<HashMap<Integer, ProductResponse>>(){}.getType();
	    HashMap<Integer, ProductResponse> clonedMap = gson.fromJson(productsJson, type);
		List<ProductSearchResponse> productSearchResponse = productsService.findProductSimilarity(clonedMap.get(Integer.valueOf(productId)), clonedMap);
		return productSearchResponse;
	}

}
