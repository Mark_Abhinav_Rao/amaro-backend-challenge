package com.amaro.backend.challenge.product;

import java.util.Vector;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductRequest {

	   @JsonProperty("products")
	   private Vector<Products> products;

	public Vector<Products> getProducts() {
		return products;
	}

	public void setProducts(Vector<Products> products) {
		this.products = products;
	} 
	
}
