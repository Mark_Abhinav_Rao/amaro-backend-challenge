package com.amaro.backend.challenge.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.amaro.backend.challenge.product.Products;
import com.amaro.backend.challenge.product.ProductResponse;
import com.amaro.backend.challenge.product.ProductSearchResponse;
import com.amaro.backend.challenge.service.ProductsService;


@Service
public class DefaultProductsService implements ProductsService {

	private static final List<String> productCharacteristics = createList();
	private static final int vectorTrue=1;
	private static final int vectorFalse=0;

	private static List<String> createList() {
		List<String> productCharacteristics = new ArrayList<String>();
		productCharacteristics.add("neutro");
		productCharacteristics.add("veludo");
		productCharacteristics.add("couro");
		productCharacteristics.add("basics");
		productCharacteristics.add("festa");
		productCharacteristics.add("workwear");
		productCharacteristics.add("inverno");
		productCharacteristics.add("boho");
		productCharacteristics.add("estampas");
		productCharacteristics.add("balada");
		productCharacteristics.add("colorido");
		productCharacteristics.add("casual");
		productCharacteristics.add("liso");
		productCharacteristics.add("moderno");
		productCharacteristics.add("passeio");
		productCharacteristics.add("metal");
		productCharacteristics.add("viagem");
		productCharacteristics.add("delicado");
		productCharacteristics.add("descolado");
		productCharacteristics.add("elastano");
		return productCharacteristics;
	}

	/*method to create the product characteristics vectors for each product. A static list of 
	*20 characteristics is maintained. Based on the JSON input, vector tags of size 20 are created.
	*/
	@Override
	public Map<String,ProductResponse> findProductTags(List<Products> productRequests) {
		 Map<String,ProductResponse> responses = new  HashMap<String,ProductResponse>();
		if(!CollectionUtils.isEmpty(productRequests))
		{
			productRequests.forEach(productRequest->{
				ProductResponse productResponse = new ProductResponse();
				Vector<Integer> vectorTags = new Vector<Integer>(20);
				Vector<String> tags = productRequest.getTags();
				productCharacteristics.forEach((item)->{
					if(tags.contains(item)) {
						vectorTags.add(vectorTrue);
					}else {
						vectorTags.add(vectorFalse);
					}
				});
				productResponse.setTagsVector(vectorTags);
				productResponse.setId(productRequest.getId());
				productResponse.setName(productRequest.getName());
				productResponse.setTags(productRequest.getTags());
				responses.put(productRequest.getId(),productResponse);
			});
		}
		return responses;
	}

	/*method to find the similarity between the searched product and the remaining products and
	 * return the 3 most similar products to the searched product. 
	 */
	@Override
	public List<ProductSearchResponse> findProductSimilarity(ProductResponse searchProduct, HashMap<Integer, ProductResponse> products) {
		List<ProductSearchResponse> productsSearchResponse = new ArrayList<ProductSearchResponse>();
		Vector<Integer> searchProductVector = searchProduct.getTagsVector();
		products.remove(Integer.valueOf(searchProduct.getId()));
		products.forEach((id,product)->{
			ProductSearchResponse productSearchResponse = new ProductSearchResponse();
			int position = searchProductVector.size()-1;
			double totalDistance = 0.0;
			Vector<Integer> productTagsVector = product.getTagsVector();
			while(position>0)
			{
				totalDistance += Math.pow(searchProductVector.get(position)-productTagsVector.get(position),2 ); 	
				position--;
			}
			double distance = Math.sqrt(totalDistance);
			double similarity = 1/(1+distance);
			productSearchResponse.setId(product.getId());
			productSearchResponse.setName(product.getName());
			productSearchResponse.setSimilarity(similarity);
			productsSearchResponse.add(productSearchResponse);
		});
		Collections.sort(productsSearchResponse);
		return productsSearchResponse.subList(0, 3);
	}
}
