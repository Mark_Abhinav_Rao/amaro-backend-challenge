package com.amaro.backend.challenge.product;

import java.util.Vector;

public class Products {

	String id;
	String name;
	Vector<String> tags = new Vector<String>();
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Vector<String> getTags() {
		return tags;
	}
	public void setTags(Vector<String> tags) {
		this.tags = tags;
	}
	
}
